<?php


namespace Drupal\Teests\webform_list_decorator\Functional;


use Drupal\Core\Config\FileStorage;
use Drupal\Tests\BrowserTestBase;
use Drupal\webform\WebformInterface;

class WebformListDecoratorTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['webform', 'webform_list_decorator'];

  /**
   * Create test webform.
   *
   * @param string $id
   *   - Webform ID.
   * @return WebformInterface|null
   *   - Webform Entity.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function loadWebform(string $id): ?WebformInterface {

    $storage = \Drupal::entityTypeManager()->getStorage('webform');

    $config_name = 'webform.webform.' . $id;
    $config_directory = drupal_get_path('module', 'webform_list_decorator') . '/modules/webform_templates/config/install';
    $file_storage = new FileStorage($config_directory);
    $values = $file_storage->read($config_name);
    $webform = $storage->create($values);
    $webform->save();

    return $webform;
  }

  public function testNewWebformListDecorator() {

//    $account = $this->drupalCreateUser(['administer rules']);
//    $this->drupalLogin($account);
//
//    $webform = $this->loadWebform('webform_list_decorator_test');
//    $this->assertLi

    $this->assertEqual('test', 'test');

  }

}
