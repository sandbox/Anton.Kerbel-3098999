<?php

namespace Drupal\webform_list_decorator\ParamConverter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\webform_list_decorator\Entity\WebformListDecorator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Route;

/**
 * Class DecoratorParamConverter.
 *
 * @package Drupal\webform_list_decorator\ParamConverter
 */
class DecoratorParamConverter implements ParamConverterInterface {

  /**
   * Entity type manager which performs the upcasting in the end.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityConverter.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, $entity_repository = NULL) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $webform = $this->entityTypeManager->getStorage('webform')->load($value);
    if (!$webform) {
      throw new NotFoundHttpException();
    }

    $entity = $this->entityTypeManager->getStorage($definition['type'])->load($value);
    if (!$entity) {
      $entity = new WebformListDecorator(['id' => $value], $definition['type']);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return $definition['type'] == 'webform_list_decorator';
  }
}
