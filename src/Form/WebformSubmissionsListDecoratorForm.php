<?php

namespace Drupal\webform_list_decorator\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WebformSubmissionsListDecoratorForm
 *
 * @package Drupal\webform_list_decorator\Form
 */
class WebformSubmissionsListDecoratorForm extends EntityForm {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $webformStorage;

  /**
   * Construct
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $webform_storage
   */
  public function __construct(EntityStorageInterface $webform_storage) {
    $this->webformStorage = $webform_storage;
  }

  /**
   * Factory method
   */
  public static function create(ContainerInterface $container) {
    $form = new static($container->get('entity_type.manager')->getStorage('webform'));
    $form->setMessenger($container->get('messenger'));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $webform = $this->webformStorage->load($this->entity->id());
    $elements = $this->entity->getElements();

    $form['submission_settings']['submission_container']['elements'] = [
      '#type' => 'details',
      '#title' => $this->t('Included submission values'),
      '#description' => $this->t('Choose webform elements which must be included from submissions list.'),
      '#open' => TRUE,
    ];
    $form['submission_settings']['submission_container']['elements']['submission_excluded_elements'] = [
      '#type' => 'webform_excluded_elements',
      '#webform_id' => $webform->id(),
      '#default_value' => $elements,
    ];
    $form['back_link'] = [
      '#type' => 'link',
      '#title' => $this->t('Back to webform submission settings'),
      '#url' => Url::fromRoute('entity.webform.settings_submissions', ['webform' => $webform->id()]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->setElements($form_state->getValue('submission_excluded_elements'));
    $status = $this->entity->save();

    if ($status == SAVED_UPDATED) {
      $this->messenger()->addMessage($this->t('Webform Submissions List Decorator is saved.'));
    }
  }

  /**
   * Show the title of webform via decorator entity
   *
   * @return mixed
   */
  public function title() {
    $decorator = \Drupal::routeMatch()->getParameter('webform_list_decorator');
    $webform = $this->webformStorage->load($decorator->id());
    return $webform->get('title');
  }
}
