<?php


namespace Drupal\webform_list_decorator;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\WebformMessageManagerInterface;
use Drupal\webform\WebformRequestInterface;
use Drupal\webform\WebformSubmissionListBuilder;
use Symfony\Component\HttpFoundation\RequestStack;

class WebformListBuilderDecorator extends WebformSubmissionListBuilder {

  /**
   * WebformListBuilderDecorator constructor.
   *
   * @param EntityTypeInterface $entity_type
   * @param EntityStorageInterface $storage
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param RouteMatchInterface $route_match
   * @param RequestStack $request_stack
   * @param AccountInterface $current_user
   * @param ConfigFactoryInterface $config_factory
   * @param WebformRequestInterface $webform_request
   * @param WebformElementManagerInterface $element_manager
   * @param WebformMessageManagerInterface $message_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    EntityTypeManagerInterface $entity_type_manager,
    RouteMatchInterface $route_match,
    RequestStack $request_stack,
    AccountInterface $current_user,
    ConfigFactoryInterface $config_factory,
    WebformRequestInterface $webform_request,
    WebformElementManagerInterface $element_manager,
    WebformMessageManagerInterface $message_manager
  ) {
    parent::__construct(
      $entity_type,
      $storage,
      $entity_type_manager,
      $route_match,
      $request_stack,
      $current_user,
      $config_factory,
      $webform_request,
      $element_manager,
      $message_manager
    );

    $config = $entity_type_manager->getStorage('webform_list_decorator');
    $webform = $webform_request->getCurrentWebform();
    $decorator = $config->load($webform->id());

    $webformElementNames = array_keys($webform->getElementsDecodedAndFlattened());

    // Webform elements have prefix "element__".
    $elements = array_map(
      function($element) use ($webformElementNames) {
        if (in_array($element, $webformElementNames)) {
          return 'element__' . $element;
        }
        else {
          return $element;
        }
      },
      $decorator->getElements()
    );

    foreach (array_keys($this->columns) as $column) {
      if (in_array($column, $elements)) {
        unset($this->columns[$column]);
      }
    }
  }
}
