<?php

namespace Drupal\webform_list_decorator\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Class WebformListDecorator.
 *
 * @ConfigEntityType(
 *   id = "webform_list_decorator",
 *   label = @Translation("Webform Submisions List Decorator"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\webform_list_decorator\Form\WebformSubmissionsListDecoratorForm",
 *       "edit" = "Drupal\webform_list_decorator\Form\WebformSubmissionsListDecoratorForm"
 *     }
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "elements" = "elements"
 *   }
 * )
 *
 * @package Drupal\webform_list_decorator\Entity
 */
class WebformListDecorator extends ConfigEntityBase {

  /**
   * @var string
   */
  protected $id;

  /**
   * @var array
   */
  protected $elements;

  /**
   * Get excluded webform elements.
   *
   * @return array
   */
  public function getElements(): ?array {
    return $this->elements;
  }

  /**
   * Set excluded webform elements.
   *
   * @param array $elements
   * @return $this
   */
  public function setElements(array $elements): self {
    $this->elements = $elements;
    return $this;
  }

}
